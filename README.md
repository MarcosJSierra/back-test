# Nabenik's Enterprise Java basic test

Hi and welcome to this test. As many technical interviews, main test objective is to establish your actual Enterprise coding skills, being:

* Java knowledge
* DDD knowledge
* General toolkits, SDK's and other usages
* Jakarta EE general skills

To complete this test, please create a fork of this repository, fix the source code if required, add your answers to this readme file, commit the solutions/answers to YOUR copy and create a pull request to the original repo.

This document is structured using [GitHub Markdown Flavor](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code).

## General questions

1. How to answer these questions?

> Like this

Or maybe with code

```kotlin
fun hello() = "world"
```

2. Please describe briefly the main purpose for the following Jakarta EE specs, also add in your answer http links to actual implementations. Is this project using all specs?
>Yes, this project are using all specs. 
- EJB
> The Enterprise JavaBeans contains the business logic that operates on te data. This componentes are used in the development and deployment of distributed applications.
- Servlet
> A servlet is a web component used to generate dynamic content and is hosted in a servlet container. A client can interact with a servlet with a request/response pattern.
- CDI
> CDI (Context and Dependency Injection) defines a secure mechanism for the dependency injecton in the Java EE platform. We can see the definition of the properties on [beans.xml](./src/main/webapp/WEB-INF/beans.xml). And we can see the injection with de @Inject annotation in [Controller](./src/main/java/com/nabenik/controller/MovieController.java), and in the [repository](./src/main/java/com/nabenik/repository/MovieRepository.java).
- JAX-RS
> JAX-RS defines a standard annotation API to help build a RESTful web service in Java. This contains annotations like @Get, @Post, @Put, @Delete that helps to work with HTTP Requests. This can be see in [Controller](./src/main/java/com/nabenik/controller/MovieController.java) with the annotations in the endpoints.We can see the configuration file on the file [JAXRSConfiguration.java](./src/main/java/com/nabenik/rest/JAXRSConfiguration.java)

3. Which of the following is an application server?

* Open Liberty
* Apache TomEE
* Eclipse Jetty
* Eclipse Glassfish
* Oracle Weblogic

> Apache TomEE, Eclipse Glassfish and Oracle Weblogic are Application Servers

4. In your opinion, what's the main benefit of moving from Java 11 to Java 17?
> The introduction of Records that can help to create inmutable data classes, The introudction of Saled classes that helps to have more control about wich classes can extend of a class and the text blocks that makes more easy to work with JSON and make the code more readable.

5. Is it possible to run this project (as is) over Java 17? Why?

> This is possible because Java have a strong retrocompatibility with other versions, this make easy to work with components that use other versions of Java but is important to see if is there any deprecated method that can create any problem in the future.

6. Is it possible to run this project with GraalVM Native? Why?

7. How do you run this project directly from CLI without configuring any application server? Why is it possible?

> It's possible if we inlcudes al the libreries and dependcies that are necessary to run the project.

## Development tasks

To solve this questions please use Gitflow workflow, still, your answers should be in the current branch.

Please also include screenshots on every task. You don't need to execute every task to submit your pull request but feel free to do it :).

0. (easy) Show your terminal demonstrating your installation of OpenJDK 11

![Terminal](./static/easy_00.png)

1. (easy) Run this project using an IDE/Editor of your choice

![ejecutando](./static/easy_01_01.png)

2. (medium) Execute the movie endpoint operations using curl, if needed please also check/fix the code to be REST compliant

3. (medium) Write an SPA application using Angular, the application should be a basic CRUD that uses Movie operations, upload this application to a new Bitbucket repo and include the link as answer

4. (medium) This project has been created using Java EE APIs, please move it to Jakarta EE APIs and switch it to a compatible implementation (if needed)

5. (hard) Please identify the Integration Test for `MovieRepository`, after that implement each of the non-included CRUD methods

6. (hard) Please write the Repository and Controller for Actor model, after that implement each of the CRUD methods using an Integration Test

7. (hard) This project uses vanilla Java EE for Database Persistence, please integrate Testcontainers with MySQL for testing purposes

8. (nightmare) This source code includes only Java EE APIs, hence it's possible to port it to [Open Liberty](https://openliberty.io/). Do it and don't port Integration Tests 

